import {
  FactoryContractActionTypes,
  FactoryContractStateType,
} from "../types/FactoryContractTypes";

const initialStateFactoryContract: FactoryContractStateType = {
  address: "",
};

export const getFactoryContractReducer = (
  state = initialStateFactoryContract,
  action: FactoryContractActionTypes
): FactoryContractStateType => {
  switch (action.type) {
    default:
      return state;
  }
};
