import { combineReducers } from "redux";
import { walletReducer } from "./walletReducer";
import { getFactoryContractReducer } from "./factoryContractReducer";
import { getFundContractReducer } from "./fundContractReducer";

const rootReducer = combineReducers({
  wallet: walletReducer,
  factoryContract: getFactoryContractReducer,
  fundContract: getFundContractReducer,
});

export default rootReducer;
