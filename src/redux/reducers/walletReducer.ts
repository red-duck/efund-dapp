import {
  GET_WALLET,
  WalletStateType,
  WalletActionTypes,
} from "../types/WalletTypes";

const initialStateWallet: WalletStateType = {
  wallet: {},
};

export const walletReducer = (
  state = initialStateWallet,
  action: WalletActionTypes
): WalletStateType => {
  switch (action.type) {
    case GET_WALLET:
      return {
        ...state,
        wallet: action.payload,
      };
    default:
      return state;
  }
};
