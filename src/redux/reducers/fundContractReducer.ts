import {
  FundContractActionTypes,
  FundContractStateType,
  GET_IS_MANAGER,
  GET_MANAGER,
  GET_STATUS,
  GET_FUND_CONTRACT_ADDRESS,
} from "../types/FundContractTypes";

const initialStateFundContract: FundContractStateType = {
  fund: {},
};

export const getFundContractReducer = (
  state = initialStateFundContract,
  action: FundContractActionTypes
): FundContractStateType => {
  switch (action.type) {
    case GET_FUND_CONTRACT_ADDRESS:
      return {
        fund: {
          ...state.fund,
          address: action.payload,
        },
      };

    case GET_STATUS:
      return {
        fund: {
          ...state.fund,
          status: action.payload,
        },
      };

    case GET_MANAGER:
      return {
        fund: {
          ...state.fund,
          manager: action.payload,
        },
      };

    case GET_IS_MANAGER:
      return {
        fund: {
          ...state.fund,
          isManager: action.payload,
        },
      };

    default:
      return state;
  }
};
