// import { Dispatch } from "redux";
import { ABI, CONTRACT_ADDRESS } from "../../services/ether";
// import { Contract, ethers, providers } from "ethers";
// import { FactoryContractAction } from "../actions/FactorycontractActions";
// import { FactoryContractActionTypes } from "../types/FactoryContractTypes";
// import { Wallet } from "../interfaces";
// import { FundContractActionTypes } from "../types/FundContractTypes";
// import HedgeFund from "../../artifacts/contracts/HedgeFund.sol/HedgeFund.json";
// import { getFundContractReducer } from "../reducers/fundContractReducer";
// import {
//   FundIsManagerAction,
//   FundManagerAction,
//   FundStatusAction,
// } from "../actions/FundcontractActions";
// import { fundStatuses } from "../../components/Fund/Fund";

// const FUND_ABI = JSON.stringify(HedgeFund.abi);

// export const loadFundContract = (wallet: Wallet, contractAddress: string) => {
//   return async function (
//     dispatch: Dispatch<FundContractActionTypes>
//   ): Promise<Contract | boolean> {
//     const fundContract = await new ethers.Contract(
//       contractAddress,
//       FUND_ABI,
//       wallet.signer
//     );
//
//     const status = await fundContract?.fundStatus();
//     console.log("status", { status });
//
//     dispatch(FundContractAction(fundContract));
//
//     fundContract.functions
//       ?.fundStatus()
//       .then((res: number[]) => {
//         console.log("fundStatus", res);
//         dispatch(FundStatusAction(fundStatuses[res[0]].value));
//       })
//       .catch((e: any) => console.log);
//
//     fundContract?.functions
//       .fundManager()
//       .then((res: number[]) => {
//         dispatch(FundManagerAction(res[0].toString()));
//         dispatch(
//           FundIsManagerAction(
//             wallet?.currentAddress?.toLowerCase() ==
//               res[0].toString().toLowerCase()
//           )
//         );
//       })
//       .catch((e) => console.log);
//
//     return fundContract;
//   };
// };

// export const setStatusFundContract = () => {
//   return async function () {};
// };
