import { FundStatus } from "../../components/Fund/Fund";

export interface Wallet {
  currentAddress?: string;
}
export interface FactoryContract {
  address?: string;
}

export interface FundContract {
  address?: string;
  status?: FundStatus;
  manager?: string;
  isManager?: boolean;
}
