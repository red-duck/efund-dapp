import { FundContract } from "../interfaces";
import { FundStatus } from "../../components/Fund/Fund";

export const GET_FUND_CONTRACT_ADDRESS = "GET_FUND_CONTRACT_ADDRESS";
export const GET_STATUS = "GET_STATUS";
export const GET_MANAGER = "GET_MANAGER";
export const GET_IS_MANAGER = "GET_IS_MANAGER";

export interface FundContractStateType {
  fund: FundContract;
}

interface GetFundContractAddressActionType {
  type: typeof GET_FUND_CONTRACT_ADDRESS;
  payload: string;
}

interface GetFundContractStatusActionType {
  type: typeof GET_STATUS;
  payload: FundStatus;
}

interface GetFundContractManagerActionType {
  type: typeof GET_MANAGER;
  payload: string;
}

interface GetFundContractIsManagerActionType {
  type: typeof GET_IS_MANAGER;
  payload: boolean;
}

export type FundContractActionTypes =
  | GetFundContractAddressActionType
  | GetFundContractStatusActionType
  | GetFundContractManagerActionType
  | GetFundContractIsManagerActionType;
