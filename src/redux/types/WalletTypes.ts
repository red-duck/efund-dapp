import { Wallet } from "../interfaces";

export const GET_WALLET = "GET_WALLET";

export interface WalletStateType {
  wallet?: Wallet;
}

interface GetWalletActionType {
  type: typeof GET_WALLET;
  payload: Wallet;
}
export type WalletActionTypes = GetWalletActionType;
