export const GET_FACTORY_CONTRACT_ADDRESS = "GET_FACTORY_CONTRACT_ADDRESS";

export interface FactoryContractStateType {
  address?: string;
}

interface GetFactoryContractAddressActionType {
  type: typeof GET_FACTORY_CONTRACT_ADDRESS;
  payload: string;
}

export type FactoryContractActionTypes = GetFactoryContractAddressActionType;
