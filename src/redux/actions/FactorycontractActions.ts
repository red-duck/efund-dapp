import {
  FactoryContractActionTypes,
  GET_FACTORY_CONTRACT_ADDRESS,
} from "../types/FactoryContractTypes";

export const FactoryContractAction = (
  address: string
): FactoryContractActionTypes => {
  return {
    type: GET_FACTORY_CONTRACT_ADDRESS,
    payload: address,
  };
};
