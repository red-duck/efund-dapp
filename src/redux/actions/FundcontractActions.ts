import {
  FundContractActionTypes,
  GET_FUND_CONTRACT_ADDRESS,
  GET_MANAGER,
  GET_STATUS,
  GET_IS_MANAGER,
} from "../types/FundContractTypes";
import { FundStatus } from "../../components/Fund/Fund";

export const FundContractAddressAction = (
  address: string
): FundContractActionTypes => {
  return {
    type: GET_FUND_CONTRACT_ADDRESS,
    payload: address,
  };
};

export const FundStatusAction = (
  status: FundStatus
): FundContractActionTypes => {
  return {
    type: GET_STATUS,
    payload: status,
  };
};

export const FundManagerAction = (manager: string): FundContractActionTypes => {
  return {
    type: GET_MANAGER,
    payload: manager,
  };
};

export const FundIsManagerAction = (
  isManager: boolean
): FundContractActionTypes => {
  return {
    type: GET_IS_MANAGER,
    payload: isManager,
  };
};
