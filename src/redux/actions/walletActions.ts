import { GET_WALLET, WalletActionTypes } from "../types/WalletTypes";
import { Wallet } from "../interfaces";

export const walletAction = (wallet: Wallet): WalletActionTypes => {
  return {
    type: GET_WALLET,
    payload: wallet,
  };
};
