import React, { useState } from "react";

export type ValueType = string | number;

export interface OptionItemProps {
  value: string;
  label?: string;
  disabled?: boolean;
}

export interface SelectProps {
  list: OptionItemProps[];
  value?: string;
  onChange?: (value: string) => void;
  disabled?: boolean;
  placeholder?: string;
}

export function Select({
  list,
  value: defaultValue,
  onChange: handleChange,
  disabled: isDisabled,
  placeholder,
}: SelectProps): JSX.Element {
  const [selectedValue, setSelectedValue] = useState(defaultValue);

  const onChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    console.log(e.currentTarget.value);
    setSelectedValue(e.currentTarget.value);
    handleChange && handleChange(e.currentTarget.value);
  };

  const renderOption = (option: OptionItemProps, index: number) => {
    return (
      <option value={option.value} key={index}>
        {option.label}
      </option>
    );
  };

  return (
    <select disabled={isDisabled} value={selectedValue} onChange={onChange}>
      {list.map(renderOption)}
    </select>
  );
}
