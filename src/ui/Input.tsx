import React, { useState } from "react";

interface InputProps {
  type?: string;
  name?: string;
  value?: string;
  onChange?: (value: string) => void;
  label?: string;
}

type ChangeEvent =
  | React.ChangeEvent<HTMLInputElement>
  | React.ChangeEvent<HTMLTextAreaElement>;

export const Input: React.FC<InputProps> = ({
  value: defaultValue = "",
  type = "text",
  name = "",
  onChange: handleChange,

  label = "",
}) => {
  const [value, setValue] = useState(defaultValue);

  const onChange = (e: ChangeEvent): void => {
    const { value: enteredValue } = e.target;
    setValue(enteredValue);
    handleChange && handleChange(enteredValue);
  };

  return (
    <label>
      {label}
      <input
        type={type}
        value={value}
        name={name}
        onChange={onChange}
        className="px-3 py-2 rounded-lg border-2 border-gray-200 outline-none focus:border-indigo-500"
      />
    </label>
  );
};
