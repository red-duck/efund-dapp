import React from "react";

interface ButtonProps {
  onClick: () => void;
  disabled?: boolean;
}

export const Button: React.FC<ButtonProps> = ({
  children,
  onClick,
  disabled,
}) => {
  const handleClick = () => {
    onClick && onClick();
  };
  return (
    <button
      onClick={handleClick}
      disabled={disabled}
      className={
        "w-full max-w-xs ml-3  bg-indigo-500 hover:bg-indigo-700 focus:bg-indigo-700 text-white rounded-lg px-3 py-3 font-semibold"
      }
    >
      {children}
    </button>
  );
};
