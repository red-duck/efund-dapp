import React, { useEffect, useState } from "react";
import { BigNumber, Contract, ethers } from "ethers";
import { Button } from "../../ui/Button";
import { useSelector } from "react-redux";
import { AppState } from "../../redux/store";
import {
  getReadOnlyFundContract,
  getSignedFundContract,
} from "../../services/fundService";

interface FundInfoProps {
  address: string;
}

export const FundInfo: React.FC<FundInfoProps> = ({ address }) => {
  const [balance, setBalance] = useState<string>();
  const [duration, setDuration] = useState<string>();
  const [fundReadOnlyContract, setFundReadOnlyContract] = useState<Contract>();
  const [fundSignedContract, setFundsSignedContract] = useState<Contract>();
  const walletAddress = useSelector((state: AppState) => state.wallet);

  const fundContractAddress = useSelector(
    (state: AppState) => state.fundContract.fund.address
  );

  const status = useSelector(
    (state: AppState) => state.fundContract.fund.status
  );
  const manager = useSelector(
    (state: AppState) => state.fundContract.fund.manager
  );
  const isManager = useSelector(
    (state: AppState) => state.fundContract.fund.isManager
  );

  useEffect(() => {
    async function fetchContract() {
      let contract;
      if (fundContractAddress) {
        if (walletAddress.wallet?.currentAddress) {
          contract = await getSignedFundContract(fundContractAddress);
          setFundsSignedContract(contract);
        } else {
          contract = await getReadOnlyFundContract(fundContractAddress);
          setFundReadOnlyContract(contract);
        }
      }
    }

    fetchContract();
  }, [walletAddress.wallet?.currentAddress]);

  // useEffect(() => {
  //get fund contract from

  // const interval = setInterval(() => updateBalance(), 60000);
  // return (): void => clearInterval(interval);
  // }, [fundContractAddress]);

  useEffect(() => {
    // updateBalance();
    //
    // fundSignedContract
    //   ?.fundDurationMonths()
    //   .then((res: number[]) => {
    //     setDuration(res[0].toString());
    //   })
    //   .catch(() => console.log);
  }, [fundSignedContract]);

  // const updateBalance = (): void => {
  //   fundContract?.functions
  //     .getCurrentBalanceInWei()
  //     .then((res: BigNumber[]) => {
  //       setBalance(ethers.utils.formatEther(res[0]));
  //     })
  //     .catch(() => console.log);
  // };
  //
  const setFundStatusActive = () => {
    fundSignedContract?.setFundStatusActive();
  };

  return (
    <ul>
      <li className="py-2">
        Status: {status}
        {manager && status && isManager && (
          <Button onClick={setFundStatusActive}>set Active </Button>
        )}
      </li>
      <li className="py-2">Balance: {balance}</li>
      <li className="py-2">Duration: {duration}</li>
      <li className="py-2">Manager: {manager}</li>
    </ul>
  );
};
