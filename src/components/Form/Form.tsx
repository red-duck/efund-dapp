import React, { useEffect, useState } from "react";
import "./Form.scss";
import { ethers, Contract } from "ethers";
import { Input } from "../../ui/Input";
import { FundsList } from "../FundsList/FundsList";
import { Select } from "../../ui/Select";
import FundFactory from "../../artifacts/contracts/FundFactory.sol/FundFactory.json";
import { Button } from "../../ui/Button";
import { useSelector } from "react-redux";
import { AppState } from "../../redux/store";
import { getSignedFactoryContract } from "../../services/ether";

const UNI_SWAP_V2_ROUTER = "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D";

export const ABI = JSON.stringify(FundFactory.abi);

export const Form: React.FC = () => {
  const [etherValue, setEtherValue] = useState("0.1");
  const [month, setMonth] = useState("1");
  const monthList = [{ value: "1" }, { value: "3" }, { value: "6" }];

  const [factoryContract, setFactoryContract] = useState<Contract>();

  const walletAddress = useSelector((state: AppState) => state.wallet);

  useEffect(() => {
    async function fetchContract() {
      const response = await getSignedFactoryContract();
      setFactoryContract(response);
    }

    if (walletAddress?.wallet?.currentAddress) {
      fetchContract();
    }
  }, [walletAddress?.wallet?.currentAddress]);

  const createNewFund = async (): Promise<void> => {
    const overrides = {
      value: ethers.utils.parseEther(etherValue.toString()), // To convert Ether to Wei:
    };

    if (factoryContract) {
      const tx = await factoryContract.createFund(
        UNI_SWAP_V2_ROUTER,
        month,
        [],
        overrides
      );

      return await tx.wait();
    }
  };

  return (
    <div className="p-3">
      <div className="flex">
        <Input value={etherValue} onChange={setEtherValue} type="number" />
        <Select value={month} onChange={setMonth} list={monthList} />
        <Button
          onClick={createNewFund}
          disabled={!walletAddress?.wallet?.currentAddress}
        >
          Create New
        </Button>
      </div>
      <FundsList />
    </div>
  );
};
