import React, { useEffect, useState } from "react";

import tokens from "../../services/tokens.json";
import { useSelector } from "react-redux";
import { AppState } from "../../redux/store";
import { Input } from "../../ui/Input";
import { OptionItemProps, Select } from "../../ui/Select";

export const Trade: React.FC = () => {
  const wallet = useSelector((state: AppState) => state.wallet);

  const tokensList: OptionItemProps[] = tokens.data.map((token: any) => {
    return {
      value: token.address,
      label: token.symbol,
    };
  });

  // console.log({ "fundContract.functions": fundContract?.functions });
  const [bscForSwapValue, setBscForSwapValue] = useState<string>("0.1");
  const [tokenSwapedValue, setTokenSwapedValue] = useState<string>();
  const [tokenAddress, setTokenAddress] = useState(
    "0xba2ae424d960c26247dd6c32edc70b295c744c43"
  );

  const countTokensValue = (val: string): void => {
    console.log(val);
    // setTokenSwapedValue()
  };

  const handleTokenChange = (val: string): void => {
    setTokenAddress(val);
    countTokensValue(val);
  };

  return (
    <>
      {wallet && (
        <div>
          <div className="flex">
            <Input
              value={bscForSwapValue}
              onChange={setBscForSwapValue}
              type="number"
              label="BSC amount for swap"
            />

            <Input
              value={tokenSwapedValue}
              onChange={setTokenSwapedValue}
              type="number"
              label="Tokens amount"
            />
            <Select list={tokensList} onChange={handleTokenChange} />
          </div>
        </div>
      )}
    </>
  );
};
