import React from "react";
import { Link } from "react-router-dom";
import { ConnectWallet } from "../ConnectWallet/ConnectWallet";
import { useSelector } from "react-redux";
import { AppState } from "../../redux/store";

export const Header: React.FC = () => {
  const wallet = useSelector((state: AppState) => state.wallet);
  return (
    <header className="bg-white shadow-lg border-b-2 border-gray-400 ">
      <h1>Awesome EFund Project</h1>
      <nav className="navbar navbar-light">
        <ul className="nav navbar-nav">
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/about">About us</Link>
          </li>
        </ul>
      </nav>
      {!wallet.wallet?.currentAddress ? (
        <ConnectWallet />
      ) : (
        wallet.wallet?.currentAddress
      )}
    </header>
  );
};
