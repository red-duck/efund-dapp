import React, { useState } from "react";
import { Input } from "../../ui/Input";
import { Button } from "../../ui/Button";

interface MakeDepositProps {
  onMakeDeposit: (val: string) => void;
}

export const MakeDepositForm: React.FC<MakeDepositProps> = ({
  onMakeDeposit,
}) => {
  const [sendValue, setSendValue] = useState<string>("0.1");

  const makeDeposit = () => {
    onMakeDeposit && onMakeDeposit(sendValue);
  };
  return (
    <div className="flex mt-3  py-3">
      <Input value={sendValue} onChange={setSendValue} type="number" />
      <Button onClick={makeDeposit}> Make Deposit</Button>
    </div>
  );
};
