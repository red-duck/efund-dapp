import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import { Contract } from "ethers";
import { getReadOnlyFactoryContract } from "../../services/ether";

export const FundsList: React.FC = () => {
  const [readOnlyFactoryContract, setReadOnlyFactoryContract] =
    useState<Contract>();
  const [funds, setFunds] = useState<string[]>([]);

  useEffect(() => {
    async function fetchContract() {
      const readOnlyContract = await getReadOnlyFactoryContract();
      setReadOnlyFactoryContract(readOnlyContract);
    }

    fetchContract();
  }, []);

  useEffect(() => {
    if (readOnlyFactoryContract) {
      fetchAllFunds();
    }
  }, [readOnlyFactoryContract]);

  const fetchAllFunds = async (): Promise<any> => {
    try {
      const data = await readOnlyFactoryContract?.getAllFunds();
      setFunds(data);
      console.log(data);
    } catch (err) {
      console.log("Error: ", err);
    }
  };

  return (
    <div>
      <button onClick={fetchAllFunds}>Get all funds </button>
      <ul className="divide-y divide-gray-200">
        {funds &&
          funds.length &&
          funds.map((fund, index) => {
            return (
              <li key={index}>
                <Link to={`fund/${fund}`}>{fund}</Link>
              </li>
            );
          })}
      </ul>
    </div>
  );
};
