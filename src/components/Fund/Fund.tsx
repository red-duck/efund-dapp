import React, { useEffect } from "react";
import { ethers } from "ethers";
import { MakeDepositForm } from "../MakeDepositForm/MakeDepositForm";
import { FundInfo } from "../FundInfo/FundInfo";
import { Trade } from "../Trade/Trade";
import { useDispatch, useSelector } from "react-redux";
import { AppState } from "../../redux/store";

export enum FundStatus {
  Opened = "Opened",
  Active = "Active",
  Completed = "Completed",
  Closed = "Closed",
}

interface FundProps {
  address: string;
}
//
export const fundStatuses = [
  { value: FundStatus.Opened },
  { value: FundStatus.Active },
  { value: FundStatus.Completed },
  { value: FundStatus.Closed },
];

export const Fund: React.FC<FundProps> = ({ address }) => {
  const walletAddress = useSelector((state: AppState) => state.wallet);
  // const fundContract = useSelector(
  //   (state: AppState) => state.fundContract.fund.contract
  // );
  // const status = useSelector(
  //   (state: AppState) => state.fundContract.fund.status
  // );
  // const manager = useSelector(
  //   (state: AppState) => state.fundContract.fund.manager
  // );
  // const isManager = useSelector(
  //   (state: AppState) => state.fundContract.fund.isManager
  // );

  // useEffect(() => {
  //   if (wallet.wallet.signer && !fundContract?.functions) {
  //     dispatch(loadFundContract(wallet.wallet, address));
  //   }
  // }, [dispatch, fundContract, wallet.wallet]);
  //
  // const makeDepositToFund = (sendValue: string): void => {
  //   const overrides = {
  //     value: ethers.utils.parseEther(sendValue),
  //   };
  //   fundContract?.functions.makeDeposit(overrides);
  // };

  return (
    <>
      <div className="text-center mb-10">
        <h1 className="font-bold text-3xl text-gray-900">
          {/*{fundContract?.address}*/}
        </h1>
        <p>Fund address</p>
      </div>
      <FundInfo address={address} />
      {/*{status === FundStatus.Opened && (*/}
      {/*// <MakeDepositForm onMakeDeposit={makeDepositToFund} />*/}
      {/*)}*/}
      {/*{isManager && status === FundStatus.Active && <Trade />}*/}
    </>
  );
};
