import React, { useState } from "react";
import { NetworkErrorMessage } from "../../ui/NetworkErrorMessage";
import {
  isMetaMaskInstalled,
  currentProvider,
  getSigner,
} from "../../services/ether";
import { useDispatch } from "react-redux";
import { walletAction } from "../../redux/actions/walletActions";

export const ConnectWallet: React.FC = () => {
  const [networkError, setNetworkError] = useState<string>("");

  const dispatch = useDispatch();

  const handleConnectWallet = async () => {
    const accounts = await window.ethereum.request({
      method: "eth_requestAccounts",
    });
    if (accounts[0]) {
      const { address } = await getSigner();
      dispatch(walletAction({ currentAddress: address }));
    }

    currentProvider.on("accountsChanged", ([newAddress]: string[]) => {
      if (newAddress === undefined) {
        setNetworkError("");
      }
      dispatch(walletAction({ currentAddress: newAddress }));
    });

    currentProvider.on("chainIdChanged", async ([networkId]: string[]) => {
      setNetworkError("");
      const { address } = await getSigner();
      dispatch(walletAction({ currentAddress: address }));
    });
  };

  const dismissNetworkError = () => {
    // setNetworkError("");
  };

  return (
    <div className=" p-4 text-center">
      {networkError ? (
        <div className="col-12 text-center">
          <NetworkErrorMessage
            message={networkError}
            dismiss={dismissNetworkError}
          />
        </div>
      ) : (
        <>
          <p>Please connect to your wallet.</p>
          <button
            type="button"
            onClick={handleConnectWallet}
            disabled={!isMetaMaskInstalled()}
          >
            Connect Wallet
          </button>
        </>
      )}
    </div>
  );
};
