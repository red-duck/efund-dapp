import { Contract, ethers } from "ethers";
import { ABI, currentProvider, getSigner } from "./ether";

export const getSignedFundContract = async (
  address: string
): Promise<Contract> => {
  const { jsonSigner } = await getSigner();
  return new ethers.Contract(address, ABI, jsonSigner);
};

export const getReadOnlyFundContract = async (
  address: string
): Promise<Contract> => {
  return new ethers.Contract(address, ABI, currentProvider);
};
