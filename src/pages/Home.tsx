import React, { useEffect } from "react";
import { Form } from "../components/Form/Form";
import { startApp } from "../services/ether";

export const Home: React.FC = () => {
  useEffect(() => {
    startApp();
  }, []);

  return (
    <div>
      <Form />
    </div>
  );
};
