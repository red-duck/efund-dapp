import React from "react";
import { useParams } from "react-router";
import { Fund as FundComponent } from "../components/Fund/Fund";
import { useDispatch } from "react-redux";
import { FundContractAddressAction } from "../redux/actions/FundcontractActions";

export const Fund: React.FC = () => {
  const { fundId: fundAddress } = useParams<{ fundId: string }>();
  const dispatch = useDispatch();

  dispatch(FundContractAddressAction(fundAddress));

  return <FundComponent address={fundAddress} />;
};
