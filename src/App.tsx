import React from "react";
import "./App.css";
import "tailwindcss/tailwind.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Home } from "./pages/Home";
import { About } from "./pages/About";
import { Fund } from "./pages/Fund";
import { Header } from "./components/Header/Header";
import store from "./redux/store";

export const App: React.FC = () => {
  return (
    <BrowserRouter>
      <div className="App bg-gradient-to-br from-gray-400 to-white min-h-screen overflow-auto">
        <Header />
        <div className="containstoreer max-w-6xl mx-auto px-4">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/about" component={About} />
            <Route exact path="/fund/:fundId" component={Fund} />
          </Switch>
        </div>
      </div>
    </BrowserRouter>
  );
};
